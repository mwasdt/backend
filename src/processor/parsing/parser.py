import json

from database.model import Test, Observation
from repository import dipole_repository as dipole_repo
from repository import tile_repository as tile_repo


def parseAllData(all_data):
    """ Iterates over all_data and returns a list of Test objects """
    test_list = []

    # create the list
    for receiver in range(16):
        for slot in range(8):
            test = Test()
            test_list.append(test)

            # get associated tile_id
            test.tile_id = tile_repo.getTileID(receiver, slot)

            # build observations
            for obs_no in range(20):
                for polarity in range(2):
                    obs = Observation()

                    # get start time
                    obs.start_time = all_data[obs_no][0]

                    # get name
                    obs.name = all_data[obs_no][1]

                    # get correct dipole_id
                    obs.dipole_id = dipole_repo.getDipoleID(test.tile_id, obs_no, polarity)

                    # get values (one for each frequency channel)
                    values = []
                    for channel in range(256):
                        data = all_data[obs_no][2]
                        values.append(float(data[receiver][slot][polarity][channel]))

                    obs.values = json.dumps(values)

                    # add the observation to the test
                    test.observations.append(obs)

            # get the earliest start time for the test
            test.start_time = test.observations[0].start_time
            for obs in test.observations:
                test.start_time = min((test.start_time, obs.start_time))

    return test_list
