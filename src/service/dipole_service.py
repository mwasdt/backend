import json

from flask import Response

from repository import dipole_repository as repo
from view_model.dipole_view import DipoleView


def getDipoleAll():
    """ Get all Dipoles """
    resp = Response()
    dipoles = repo.getAll()

    data = []
    for dipole in dipoles:
        data.append(DipoleView(dipole=dipole).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def getDipoleByID(dipole_id):
    """ Get a single Dipole that has a matching ID """
    resp = Response()
    dipole = repo.getByID(dipole_id)

    if not dipole:
        resp.status_code = 404
    else:
        data = DipoleView(dipole=dipole).__dict__

        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp
