import json

from flask import Response

from database.model import Anomaly
from repository import anomaly_repository as repo
from view_model.anomaly_view import AnomalyView


def getAnomalyAll():
    """ Get all Anomalies """
    resp = Response()
    anomalies = repo.getAll()

    data = []
    for anomaly in anomalies:
        data.append(AnomalyView(anomaly=anomaly).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def getAnomalyByID(anom_id):
    """ Get a single Anomaly that has a matching ID """
    resp = Response()
    anomaly = repo.getByID(anom_id)

    if not anomaly:
        resp.status_code = 404
    else:
        data = AnomalyView(anomaly=anomaly).__dict__
        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def postAnomaly(request):
    """ Post a single Anomaly """
    resp = Response()
    data = Anomaly(
        type=request.form['type'],
        observation=request.form['observation']
    )
    anomaly_id = repo.create(data)

    resp.data = json.dumps({'id': anomaly_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def putAnomalyByID(request, anom_id):
    """ Put a single Anomaly """
    query = repo.getByID(anom_id)
    resp = Response()

    if not query:
        # create the Anomaly if it doesn't exist
        # and return its id
        resp = postAnomaly(request)

    else:
        # update Anomaly
        data = AnomalyView(dict=request.form)
        data.id = anom_id
        repo.update(data)
        resp.status_code = 200

    return resp


def deleteAnomalyByID(anom_id):
    """ Delete a single Anomaly if it exists """
    resp = Response()

    repo.deleteByID(anom_id)
    resp.status_code = 200

    return resp

