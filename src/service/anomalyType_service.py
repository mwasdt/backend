import json

from flask import Response

from database.model import AnomalyType
from repository import anomalyType_repository as repo
from view_model.anomalyType_view import AnomalyTypeView


def getAnomalyTypeAll():
    """ Get all AnomalyTypes """
    resp = Response()
    anomaly_types = repo.getAll()

    data = []
    for anomaly_type in anomaly_types:
        data.append(AnomalyTypeView(anom_type=anomaly_type).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def getAnomalyTypeByID(type_id):
    """ Get a single AnomalyType that has a matching ID """
    resp = Response()
    anomaly_type = repo.getByID(type_id)

    if not anomaly_type:
        resp.status_code = 404
    else:
        data = AnomalyTypeView(anom_type=anomaly_type).__dict__
        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def postAnomalyType(request):
    """ Post a single AnomalyType """
    resp = Response()
    data = AnomalyType(
        name=request.form['name'],
        description=request.form['description']
    )
    anomaly_type_id = repo.create(data)

    resp.data = json.dumps({'id': anomaly_type_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def putAnomalyTypeByID(request, type_id):
    """ Put a single AnomalyType """
    query = repo.getByID(type_id)
    resp = Response()

    if not query:
        # create the AnomalyType if it doesn't exist
        # and return its id
        resp = postAnomalyType(request)

    else:
        # update AnomalyType
        data = AnomalyTypeView(dict=request.form)
        data.id = type_id
        repo.update(data)
        resp.status_code = 200

    return resp


def deleteAnomalyTypeByID(type_id):
    """ Delete a single AnomalyType if it exists """
    resp = Response()

    repo.deleteByID(type_id)
    resp.status_code = 200

    return resp

