import json

from flask import Response

from database.model import Test, Observation
from repository import test_repository as repo
from view_model.observation_view import ObservationView
from view_model.test_view import TestView


def getTestAll(get_obs=False):
    """ Get all Tests.
        Pass get_obs as True to return the observations associated with the test.
    """
    resp = Response()
    tests = repo.getAll(get_obs)

    data = []
    for test in tests:
        obs_list = []
        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data.append({
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        })

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def getTestByID(test_id, get_obs=False):
    """ Get a single Test that has a matching ID.
        Pass get_obs as True to return the observations associated with the test.
    """
    resp = Response()
    test = repo.getByID(test_id, get_obs)

    if not test:
        resp.status_code = 404
    else:
        obs_list = []
        if get_obs == 'true':
            for obs in test.observations:
                obs_list.append(ObservationView(obs=obs).__dict__)

        data = {
            'id': test.id,
            'tile_id': test.tile_id,
            'start_time': test.start_time,
            'observations': obs_list
        }

        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def postTest(request):
    """ Post a single Test """
    resp = Response()

    # build list of observations
    obs_list = []
    for obs in json.loads(request.form['observations']):
        obs_list.append(Observation(
            name=obs['name'],
            start_time=obs['start_time'],
            dipole_id=obs['dipole_id'],
            values=obs['values'],
        ))

    # build test object
    data = Test(
        tile_id=request.form['tile_id'],
        start_time=request.form['start_time'],
        observations=obs_list
    )
    test_id = repo.create(data)

    resp.data = json.dumps({'id': test_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def putTestByID(request, test_id):
    """ Put a single Test. Does not update observations. """
    query = repo.getByID(test_id)
    resp = Response()

    if not query:
        # create the Test if it doesn't exist
        # and return its id
        resp = postTest(request)

    else:
        # update Test #
        data = TestView(dict=request.form)
        data.id = test_id
        repo.update(data)
        resp.status_code = 200

    return resp


def deleteTestByID(test_id):
    """ Delete a single Test if it exists """
    resp = Response()

    repo.deleteByID(test_id)
    resp.status_code = 200

    return resp
