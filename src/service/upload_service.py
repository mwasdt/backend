import os
from cPickle import UnpicklingError

from flask import Response
from werkzeug.utils import secure_filename

from app import app_config
from processor.parsing.dptest import loadFile
from processor.parsing.parser import parseAllData
from repository import test_repository as test_repo


def uploadFile(request):
    """ serves a file upload request, starts processing the file if it is valid,
        removes the file before the request returns
    """
    resp = Response()

    # make sure there is actually a file
    if 'file' not in request.files:
        resp.status_code = 400
        resp.data = 'Ensure the \'file\' field is set.'
        return resp

    # get the file
    file = request.files['file']

    # if file is valid
    if file and file.filename != '' and allowedFile(file.filename):
        # sanitize file name
        filename = secure_filename(file.filename)

        # save file
        file_path = os.path.join(
            app_config[os.getenv('APP_SETTINGS')].UPLOAD_FOLDER,
            filename)
        file.save(file_path)

        # start processing file
        if processPickleFile(file_path):
            resp.status_code = 200
        else:
            resp.status_code = 400
            resp.data = 'UnpicklingError: ensure the uploaded file is in the expected format'

        # remove file from disk
        file.close()
        os.remove(file_path)

    else:
        resp.status_code = 400
        resp.data = 'File is invalid.'

    return resp


def allowedFile(filename):
    """ checks file extension against set of allowed extensions """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in \
           app_config[os.getenv('APP_SETTINGS')].ALLOWED_EXTENSIONS


def processPickleFile(filename):
    """ attempts to unpickle filename, parse its data into the database models,
        do anomaly detection, and post data and anomaly detection results,
        to the database
        returns True on success, else False
    """
    try:
        # unpickle filename
        all_data, dstate = loadFile(filename)
    except UnpicklingError:
        return False

    # parse to database models
    test_list = parseAllData(all_data)

    # TODO - anomaly detection here, add found anomalies

    # push to database
    test_repo.createAll(test_list)

    return True
