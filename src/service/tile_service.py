import json

from flask import Response

from repository import tile_repository as repo
from view_model.tile_view import TileView


def getTileAll():
    """ Get all Tiles """
    resp = Response()
    tiles = repo.getAll()

    data = []
    for tile in tiles:
        data.append(TileView(tile=tile).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def getTileByID(tile_id):
    """ Get a single Tile that has a matching ID """
    resp = Response()
    tile = repo.getByID(tile_id)

    if not tile:
        resp.status_code = 404
        return resp

    data = TileView(tile=tile).__dict__
    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp
