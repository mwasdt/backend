import json

from flask import Response

from database.model import Observation
from repository import observation_repository as repo
from view_model.observation_view import ObservationView


def getObservationAll():
    """ Get all Observations """
    resp = Response()
    observations = repo.getAll()

    data = []
    for obs in observations:
        data.append(ObservationView(obs=obs).__dict__)

    resp.data = json.dumps(data)
    resp.mimetype = 'application/json'
    resp.status_code = 200

    return resp


def getObservationByID(obs_id):
    """ Get a single Observation that has a matching ID """
    resp = Response()
    obs = repo.getByID(obs_id)

    if not obs:
        resp.status_code = 404
    else:
        data = ObservationView(obs=obs).__dict__
        resp.data = json.dumps(data)
        resp.mimetype = 'application/json'
        resp.status_code = 200

    return resp


def postObservation(request):
    """ Post a single Observation """
    resp = Response()
    data = Observation(
        name=request.form['name'],
        start_time=request.form['start_time'],
        dipole_id=request.form['dipole_id'],
        values=request.form['values']
    )
    obs_id = repo.create(data)

    resp.data = json.dumps({'id': obs_id})
    resp.mimetype = 'application/json'
    resp.status_code = 201

    return resp


def putObservationByID(request, obs_id):
    """ Put a single Observation """
    query = repo.getByID(obs_id)
    resp = Response()

    if not query:
        # create the Observation if it doesn't exist
        # and return its id
        resp = postObservation(request)

    else:
        # update Observation
        data = ObservationView(dict=request.form)
        data.id = obs_id
        repo.update(data)
        resp.status_code = 200

    return resp


def deleteObservationByID(obs_id):
    """ Delete a single Observation if it exists """
    resp = Response()

    repo.deleteByID(obs_id)
    resp.status_code = 200

    return resp
