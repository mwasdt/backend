import unittest

from app import createApp
from app.endpoint.tile_endpoint import tile, tileID
from database.model import Tile
from test.setup_test import databaseSetUp, databaseTearDown


class TileServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_tile_get(self):
        """ test '/tile' endpoint using http method GET """
        with self.app.test_request_context(method='GET'):
            res = tile()
            self.assertEqual(res.status_code, 200)

    def test_tileByID_get(self):
        """ test '/tile/<int:id>' endpoint using http method GET """
        session = self.app.session_factory()

        good_id = session.query(Tile).first().id
        with self.app.test_request_context(method='GET'):
            res = tileID(tile_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1
        with self.app.test_request_context(method='GET'):
            res = tileID(tile_id=bad_id)
            self.assertEqual(res.status_code, 404)
