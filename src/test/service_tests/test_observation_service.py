import json
import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import createApp
from app.endpoint.observation_endpoint import observation, observationID
from database.model import Dipole, Observation
from test.setup_test import databaseSetUp, databaseTearDown


class ObservationServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_observation_get(self):
        """ test '/observation' endpoint using http method GET """
        with self.app.test_request_context(method='GET'):
            res = observation()
            self.assertEqual(res.status_code, 200)

    def test_observation_post(self):
        """ test '/observation' endpoint using http method POST """
        session = self.app.session_factory()

        good_data = {
            'name': 'test_name',
            'start_time': 12345,
            'dipole_id': session.query(Dipole).first().id,
            'values': json.dumps([1.1, 1.2, 1.3, 1.4])
        }
        with self.app.test_request_context(method='POST', data=good_data):
            res = observation()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}
        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, observation)

        bad_value = {'name': -1, 'start_time': 'q', 'dipole_id': -1,
                     'values': json.dumps([-1, -2.4])}
        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, observation)

    def test_observationByID_get(self):
        """ test '/observation/<int:id>' endpoint using http method GET """
        session = self.app.session_factory()

        good_id = session.query(Observation).first().id
        with self.app.test_request_context(method='GET'):
            res = observationID(obs_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1
        with self.app.test_request_context(method='GET'):
            res = observationID(obs_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_observationByID_put(self):
        """ test '/observation/<int:id>' endpoint using http method PUT """
        session = self.app.session_factory()

        good_data = {
            'name': 'test_name',
            'start_time': 67890,
            'dipole_id': session.query(Dipole).first().id,
            'values': json.dumps([1.1, 1.2, 1.3, 1.4])
        }
        bad_key = {'bad': 4}
        bad_value = {'name': -1, 'start_time': 'q', 'dipole_id': -1,
                     'values': json.dumps([-1, -2.4])}

        # update
        update_id = session.query(Observation).first().id
        with self.app.test_request_context(method='PUT', data=good_data):
            res = observationID(obs_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, observationID, update_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, observationID, update_id)

        # create
        create_id = -1
        with self.app.test_request_context(method='PUT', data=good_data):
            res = observationID(obs_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, observationID, create_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, observationID, create_id)

    def test_observation_delete(self):
        """ test '/observation/<int:id>' endpoint using http method DELETE """
        with self.app.test_request_context(method='DELETE'):
            res = observationID(obs_id=1)
            self.assertEqual(res.status_code, 200)

