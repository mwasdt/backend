import unittest
from io import BytesIO

from app import createApp
from app.endpoint.upload_endpoint import upload
from service.upload_service import allowedFile
from test.setup_test import databaseSetUp, databaseTearDown


class UploadServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_upload_post(self):
        """ test '/upload' endpoint using http method POST """
        good_data = dict(
            file=(BytesIO(open('./test/sample.pickle', 'rb').read()), 'sample.pickle')
        )
        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=good_data):
            res = upload()
            self.assertEqual(res.status_code, 200)

        bad_filename = dict(
            file=(BytesIO(open('./test/sample.pickle', 'rb').read()), 'sample.bad')
        )
        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=bad_filename):
            res = upload()
            self.assertEqual(res.status_code, 400)

        bad_file = dict(
            file=(BytesIO(b'bad file contents'), 'sample.pickle')
        )
        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=bad_file):
            res = upload()
            self.assertEqual(res.status_code, 400)

        no_file = dict()
        with self.app.test_request_context(method='POST', content_type='multipart/form-data', data=no_file):
            res = upload()
            self.assertEqual(res.status_code, 400)

    def test_allowedFile(self):
        # allowed file extension
        self.assertEqual(allowedFile('test.pickle'), True)
        # disallowed file extension
        self.assertEqual(allowedFile('test.nope'), False)
