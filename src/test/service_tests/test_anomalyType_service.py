import unittest

from werkzeug.exceptions import BadRequestKeyError

from app import createApp
from app.endpoint.anomalyType_endpoint import anomalyType, anomalyTypeID
from database.model import AnomalyType
from test.setup_test import databaseSetUp, databaseTearDown


class AnomalyTypeServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_anomalyType_get(self):
        """ test '/anomaly-type' endpoint using http method GET """
        with self.app.test_request_context(method='GET'):
            res = anomalyType()
            self.assertEqual(res.status_code, 200)

    def test_anomalyType_post(self):
        """ test '/anomaly-type' endpoint using http method POST """
        good_data = {'name': 'name', 'description': 'desc'}
        with self.app.test_request_context(method='POST', data=good_data):
            res = anomalyType()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}
        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomalyType)

    def test_anomalyTypeByID_get(self):
        """ test '/anomaly-type/<int:id>' endpoint using http method GET """
        session = self.app.session_factory()

        good_id = session.query(AnomalyType).first().id
        with self.app.test_request_context(method='GET'):
            res = anomalyTypeID(type_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1
        with self.app.test_request_context(method='GET'):
            res = anomalyTypeID(type_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_anomalyTypeByID_put(self):
        """ test '/anomaly-type/<int:id>' endpoint using http method PUT """
        session = self.app.session_factory()
        good_data = {'name': 'name', 'description': 'desc'}
        bad_key = {'bad': 4}

        # update
        update_id = session.query(AnomalyType).first().id
        with self.app.test_request_context(method='PUT', data=good_data):
            res = anomalyTypeID(type_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomalyTypeID, update_id)

        # create
        create_id = -1
        with self.app.test_request_context(method='PUT', data=good_data):
            res = anomalyTypeID(type_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomalyTypeID, create_id)

    def test_anomalyTypeByID_delete(self):
        """ test '/anomaly-type/<int:id>' endpoint using http method DELETE """
        with self.app.test_request_context(method='DELETE'):
            res = anomalyTypeID(type_id=1)
            self.assertEqual(res.status_code, 200)
