import json
import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import createApp
from app.endpoint.test_endpoint import test, testID
from database.model import Test, Tile, Observation, Dipole
from test.setup_test import databaseSetUp, databaseTearDown


class TestServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_test_get(self):
        """ test '/test' endpoint using http method GET """
        with self.app.test_request_context(method='GET'):
            res = test()
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='GET', query_string={'get_obs': 'true'}):
            res = test()
            self.assertEqual(res.status_code, 200)

    def test_test_post(self):
        """ test '/test' endpoint using http method POST """
        session = self.app.session_factory()

        good_data = {
            'tile_id': session.query(Tile).first().id,
            'start_time': 44,
            'observations': json.dumps([
                Observation(name='some name', start_time=777888,
                            dipole_id=session.query(Dipole).first().id,
                            values=json.dumps([8.7, 8.8, 8.9])
                            ).toDict()
            ])
        }
        with self.app.test_request_context(method='POST', data=good_data):
            res = test()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}
        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, test)

        bad_value = {
            'tile_id': 'q',
            'start_time': -1,
            'observations': good_data['observations']
        }
        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, test)

    def test_testByID_get(self):
        """ test '/test/<int:id>' endpoint using http method GET """
        session = self.app.session_factory()

        good_id = session.query(Test).first().id
        with self.app.test_request_context(method='GET'):
            res = testID(test_id=good_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='GET', query_string={'get_obs': 'true'}):
            res = testID(test_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1
        with self.app.test_request_context(method='GET'):
            res = testID(test_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_testByID_put(self):
        """ test '/test/<int:id>' endpoint using http method PUT """
        session = self.app.session_factory()

        good_data = {
            'tile_id': session.query(Tile).first().id,
            'start_time': 44,
            'observations': json.dumps([
                Observation(name='some name', start_time=77777,
                            dipole_id=session.query(Dipole).first().id,
                            values=json.dumps([7.7, 7.8, 7.9])
                            ).toDict()
            ])
        }
        bad_key = {'bad': 4}
        bad_value = {
            'tile_id': 'q',
            'start_time': -1,
            'observations': good_data['observations']
        }

        # update
        update_id = session.query(Test).first().id
        with self.app.test_request_context(method='PUT', data=good_data):
            res = testID(test_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, testID, update_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, testID, update_id)

        # create
        create_id = -1
        with self.app.test_request_context(method='PUT', data=good_data):
            res = testID(test_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, testID, create_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, testID, create_id)

    def test_testByID_delete(self):
        """ test '/test/<int:id>' endpoint using http method DELETE """
        session = self.app.session_factory()

        with self.app.test_request_context(method='DELETE'):
            res = testID(test_id=session.query(Test).first().id)
            self.assertEqual(res.status_code, 200)
