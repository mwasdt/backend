import unittest

from sqlalchemy.exc import OperationalError
from werkzeug.exceptions import BadRequestKeyError

from app import createApp
from app.endpoint.anomaly_endpoint import anomaly, anomalyID
from database.model import AnomalyType, Observation, Anomaly
from test.setup_test import databaseSetUp, databaseTearDown


class AnomalyServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_anomaly_get(self):
        """ test '/anomaly' endpoint using http method GET """
        with self.app.test_request_context(method='GET'):
            res = anomaly()
            self.assertEqual(res.status_code, 200)

    def test_anomaly_post(self):
        """ test '/anomaly' endpoint using http method POST """
        session = self.app.session_factory()

        good_data = {
            'type': session.query(AnomalyType).first().id,
            'observation': session.query(Observation).first().id
        }
        with self.app.test_request_context(method='POST', data=good_data):
            res = anomaly()
            self.assertEqual(res.status_code, 201)

        bad_key = {'bad': 4}
        with self.app.test_request_context(method='POST', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomaly)

        bad_value = {'type': 'q', 'observation': -1}
        with self.app.test_request_context(method='POST', data=bad_value):
            self.assertRaises(OperationalError, anomaly)

    def test_anomalyByID_get(self):
        """ test '/anomaly/<int:id>' endpoint using http method GET """
        session = self.app.session_factory()

        good_id = session.query(Anomaly).first().id
        with self.app.test_request_context(method='GET'):
            res = anomalyID(anom_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1
        with self.app.test_request_context(method='GET'):
            res = anomalyID(anom_id=bad_id)
            self.assertEqual(res.status_code, 404)

    def test_anomalyByID_put(self):
        """ test '/anomaly/<int:id>' endpoint using http method PUT """
        session = self.app.session_factory()

        good_data = {
            'type': session.query(AnomalyType).first().id,
            'observation': session.query(Observation).first().id
        }
        bad_key = {'bad': 4}
        bad_value = {'type': 'q', 'observation': -1}

        # update
        update_id = session.query(Anomaly).first().id
        with self.app.test_request_context(method='PUT', data=good_data):
            res = anomalyID(anom_id=update_id)
            self.assertEqual(res.status_code, 200)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomalyID, update_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, anomalyID, update_id)

        # create
        create_id = -1
        with self.app.test_request_context(method='PUT', data=good_data):
            res = anomalyID(anom_id=create_id)
            self.assertEqual(res.status_code, 201)

        with self.app.test_request_context(method='PUT', data=bad_key):
            self.assertRaises(BadRequestKeyError, anomalyID, create_id)

        with self.app.test_request_context(method='PUT', data=bad_value):
            self.assertRaises(OperationalError, anomalyID, create_id)

    def test_anomalyByID_delete(self):
        """ test '/anomaly/<int:id>' endpoint using http method DELETE """
        with self.app.test_request_context(method='DELETE'):
            res = anomalyID(anom_id=1)
            self.assertEqual(res.status_code, 200)
