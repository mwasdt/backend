import unittest

from app import createApp
from app.endpoint.dipole_endpoint import dipole, dipoleID
from database.model import Dipole
from test.setup_test import databaseSetUp, databaseTearDown


class DipoleServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.app = createApp()
        self.client = self.app.test_client()
        databaseSetUp(self.app)

    def tearDown(self):
        databaseTearDown(self.app)

    def test_dipole_get(self):
        """ test '/dipole' endpoint using http method GET """
        with self.app.test_request_context(method='GET'):
            res = dipole()
            self.assertEqual(res.status_code, 200)

    def test_dipoleByID_get(self):
        """ test '/dipole/<int:id>' endpoint using http method GET """
        session = self.app.session_factory()

        good_id = session.query(Dipole).first().id
        with self.app.test_request_context(method='GET'):
            res = dipoleID(dipole_id=good_id)
            self.assertEqual(res.status_code, 200)

        bad_id = -1
        with self.app.test_request_context(method='GET'):
            res = dipoleID(dipole_id=bad_id)
            self.assertEqual(res.status_code, 404)

