import json

from sqlalchemy import create_engine

from app.config import app_config
from database.model import Tile, Dipole, Observation, AnomalyType, Anomaly, Base, Test


def databaseSetUp(app):
    """ Put test data in the test database. """

    session = app.session_factory()

    # build and add a Tile
    session.add(Tile(receiver_number=1, slot_number=1))
    session.flush()

    # build and add a Dipole
    session.add(Dipole(
        tile_id=session.query(Tile).first().id,
        dipole_number=1,
        polarity=1
    ))
    session.flush()

    # build an Observation
    obs = Observation(
        name='obs_name',
        start_time=1234567890,
        dipole_id=session.query(Dipole).first().id,
        values=json.dumps([1.1, 2.2, 3.3, 4.4])
    )

    # build and add a Test
    session.add(Test(
        tile_id=session.query(Tile).first().id,
        start_time=22,
        observations=[obs]
    ))
    session.flush()

    # build and add an AnomalyType
    session.add(AnomalyType(
        name='test_anomaly_name',
        description='Test anomaly description.'
    ))
    session.flush()

    # build and add an Anomaly
    session.add(Anomaly(
        type=session.query(AnomalyType).first().id,
        observation=session.query(Observation).first().id
    ))
    session.flush()

    session.commit()


def databaseTearDown(app):
    """ Empty the test database. """
    session = app.session_factory()

    # truncate database, keeping seeded Tile, Dipole and AnomalyType tables
    session.query(Test).delete()
    session.query(Observation).delete()

    session.commit()


def schemaSetUp():
    """ Create the schema on the test database. """
    engine = create_engine(app_config['test'].DATABASE_URI)
    Base.metadata.create_all(engine)


if __name__ == "__main__":
    schemaSetUp()
