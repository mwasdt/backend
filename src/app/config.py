import os


class Config(object):
    """ Parent config class """
    DEBUG = False
    CSRF_ENABLED = True
    DATABASE_URI = 'mysql://mwauser:mwaicrar@localhost:3306/mwamonitor'
    UPLOAD_FOLDER = os.path.join(os.getcwd(), 'upload')
    ALLOWED_EXTENSIONS = {'pickle'}
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # 16MiB


class DevelopmentConfig(Config):
    """ Dev config """
    DEBUG = True


class TestingConfig(Config):
    """ Test config, uses a separate database """
    DEBUG = True
    TESTING = True
    DATABASE_URI = 'mysql://mwa_test_user:mwa_test_pwd@localhost:3306/mwa_test'
    UPLOAD_FOLDER = os.path.join(os.getcwd(), '../upload')


class ProductionConfig(Config):
    """ Production config """
    DEBUG = False
    TESTING = False


app_config = {
    'dev': DevelopmentConfig,
    'test': TestingConfig,
    'prod': ProductionConfig,
}
