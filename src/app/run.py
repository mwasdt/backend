from app import createApp
from app.endpoint.anomalyType_endpoint import anomalyType_endpoints
from app.endpoint.anomaly_endpoint import anomaly_endpoints
from app.endpoint.dipole_endpoint import dipole_endpoints
from app.endpoint.observation_endpoint import observation_endpoints
from app.endpoint.test_endpoint import test_endpoints
from app.endpoint.tile_endpoint import tile_endpoints
from app.endpoint.upload_endpoint import upload_endpoints
from app.exceptions import error_handlers

# app setup
app = createApp()


# register endpoints
app.register_blueprint(anomaly_endpoints)
app.register_blueprint(anomalyType_endpoints)
app.register_blueprint(dipole_endpoints)
app.register_blueprint(observation_endpoints)
app.register_blueprint(test_endpoints)
app.register_blueprint(tile_endpoints)
app.register_blueprint(upload_endpoints)

# register exceptions
app.register_blueprint(error_handlers)


# default endpoint
@app.route('/')
def landing():
    return '<h1>MWA Monitoring; API Service Base<h1>'
