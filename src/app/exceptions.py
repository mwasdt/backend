from flask import Response, Blueprint
from sqlalchemy.exc import OperationalError, IntegrityError, InvalidRequestError
from werkzeug.exceptions import BadRequestKeyError


error_handlers = Blueprint('error_handlers', __name__)


@error_handlers.app_errorhandler(OperationalError)
def handleOperationalError(error):
    resp = Response()
    resp.status_code = 500
    resp.data = 'OperationalError: {}'.format(error)
    return resp


@error_handlers.app_errorhandler(BadRequestKeyError)
def handleBadRequestKeyError(error):
    resp = Response()
    resp.status_code = 400
    resp.data = 'BadRequestKeyError: {}'.format(error)
    return resp


@error_handlers.app_errorhandler(IntegrityError)
def handleIntegrityError(error):
    resp = Response()
    resp.status_code = 500
    resp.data = 'IntegrityError: {}'.format(error)
    return resp


@error_handlers.app_errorhandler(InvalidRequestError)
def handleInvalidRequestError(error):
    resp = Response()
    resp.status_code = 400
    resp.data = 'InvalidRequestError: {}'.format(error)
    return resp
