from flask import Blueprint, request

from service.test_service import *

test_endpoints = Blueprint('test_endpoints', __name__)


@test_endpoints.route('/test', methods=['GET', 'POST'])
def test():
    if request.method == 'GET':
        return getTestAll(get_obs=request.args.get('get_obs', default=False))
    elif request.method == 'POST':
        return postTest(request)


@test_endpoints.route('/test/<int:test_id>', methods=['GET', 'PUT', 'DELETE'])
def testID(test_id):
    if request.method == 'GET':
        return getTestByID(test_id, get_obs=request.args.get('get_obs', default=False))
    elif request.method == 'PUT':
        return putTestByID(request, test_id)
    elif request.method == 'DELETE':
        return deleteTestByID(test_id)
