from flask import Blueprint, request

from service.tile_service import getTileAll, getTileByID

tile_endpoints = Blueprint('tile_endpoints', __name__)


@tile_endpoints.route('/tile', methods=['GET'])
def tile():
    if request.method == 'GET':
        return getTileAll()


@tile_endpoints.route('/tile/<int:tile_id>', methods=['GET'])
def tileID(tile_id):
    if request.method == 'GET':
        return getTileByID(tile_id)
