from flask import Blueprint, request

from service.observation_service import *

observation_endpoints = Blueprint('observation_endpoints', __name__)


@observation_endpoints.route('/observation', methods=['GET', 'POST'])
def observation():
    if request.method == 'GET':
        return getObservationAll()
    elif request.method == 'POST':
        return postObservation(request)


@observation_endpoints.route('/observation/<int:obs_id>', methods=['GET', 'PUT', 'DELETE'])
def observationID(obs_id):
    if request.method == 'GET':
        return getObservationByID(obs_id)
    elif request.method == 'PUT':
        return putObservationByID(request, obs_id)
    elif request.method == 'DELETE':
        return deleteObservationByID(obs_id)
