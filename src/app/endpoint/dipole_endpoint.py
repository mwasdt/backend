from flask import Blueprint, request

from service.dipole_service import *

dipole_endpoints = Blueprint('dipole_endpoints', __name__)


@dipole_endpoints.route('/dipole', methods=['GET'])
def dipole():
    if request.method == 'GET':
        return getDipoleAll()


@dipole_endpoints.route('/dipole/<int:dipole_id>', methods=['GET'])
def dipoleID(dipole_id):
    if request.method == 'GET':
        return getDipoleByID(dipole_id)
