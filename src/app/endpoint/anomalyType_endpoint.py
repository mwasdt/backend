from flask import Blueprint, request

from service.anomalyType_service import *

anomalyType_endpoints = Blueprint('anomalyType_endpoints', __name__)


@anomalyType_endpoints.route('/anomaly-type', methods=['GET', 'POST'])
def anomalyType():
    if request.method == 'GET':
        return getAnomalyTypeAll()
    elif request.method == 'POST':
        return postAnomalyType(request)


@anomalyType_endpoints.route('/anomaly-type/<int:type_id>', methods=['GET', 'PUT', 'DELETE'])
def anomalyTypeID(type_id):
    if request.method == 'GET':
        return getAnomalyTypeByID(type_id)
    elif request.method == 'PUT':
        return putAnomalyTypeByID(request, type_id)
    elif request.method == 'DELETE':
        return deleteAnomalyTypeByID(type_id)
