from flask import Blueprint, request

from service.upload_service import uploadFile

upload_endpoints = Blueprint('upload_endpoints', __name__)


@upload_endpoints.route('/upload', methods=['POST'])
def upload():
    return uploadFile(request)
