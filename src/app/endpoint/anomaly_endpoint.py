from flask import Blueprint, request

from service.anomaly_service import *

anomaly_endpoints = Blueprint('anomaly_endpoints', __name__)


@anomaly_endpoints.route('/anomaly', methods=['GET', 'POST'])
def anomaly():
    if request.method == 'GET':
        return getAnomalyAll()
    elif request.method == 'POST':
        return postAnomaly(request)


@anomaly_endpoints.route('/anomaly/<int:anom_id>', methods=['GET', 'PUT', 'DELETE'])
def anomalyID(anom_id):
    if request.method == 'GET':
        return getAnomalyByID(anom_id)
    elif request.method == 'PUT':
        return putAnomalyByID(request, anom_id)
    elif request.method == 'DELETE':
        return deleteAnomalyByID(anom_id)
