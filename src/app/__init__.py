import os

from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import app_config


def createApp():
    app = Flask(__name__)

    # set config
    app.config.from_object(app_config[os.getenv('APP_SETTINGS')])

    # database connection engine
    app.engine = create_engine(app.config['DATABASE_URI'])

    # create session factory
    app.session_factory = sessionmaker(bind=app.engine, autoflush=True)

    return app
