import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import app_config
from database.model import Tile, Dipole

# connection handler
engine = create_engine(app_config[os.getenv('APP_SETTINGS')].DATABASE_URI)

# create new Session
session = sessionmaker(bind=engine)()

# seed tiles
for group in range(2):
    for receiver in range(16):
        for slot in range(8):
            session.add(Tile(receiver_number=receiver, slot_number=slot))

session.commit()

# seed dipoles
tiles = session.query(Tile)
for tile in tiles:
    for dipole in range(20):
        session.add(Dipole(tile_id=tile.id, dipole_number=dipole, polarity=0))
        session.add(Dipole(tile_id=tile.id, dipole_number=dipole, polarity=1))

session.commit()
