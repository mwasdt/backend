import os

from sqlalchemy import create_engine

from app.config import app_config
from database.model import Base

# engine to handle db connection
engine = create_engine(app_config[os.getenv('APP_SETTINGS')].DATABASE_URI)

# create/update schema on the database
Base.metadata.create_all(engine)
