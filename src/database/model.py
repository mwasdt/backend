from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.dialects.mysql import JSON, BIT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


# association object
TestObservation = Table(
    'TestObservation',
    Base.metadata,
    Column('test_id', ForeignKey('test.id', ondelete='CASCADE'), primary_key=True, index=True),
    Column('observation_id', ForeignKey('observation.id', ondelete='CASCADE'), primary_key=True, index=True)
)


# model tables #

class Anomaly(Base):
    __tablename__ = 'anomaly'
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(Integer, ForeignKey('anomaly_type.id', ondelete='CASCADE'), nullable=False)
    observation = Column(Integer, ForeignKey('observation.id', ondelete='CASCADE'), nullable=False)

    def toDict(self):
        return {
            'id': self.id,
            'type': self.type,
            'observation': self.observation
        }


class AnomalyType(Base):
    __tablename__ = 'anomaly_type'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(40), nullable=False)
    description = Column(String(400))

    anomaly = relationship('Anomaly', cascade='all, delete-orphan')

    def toDict(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }


class Dipole(Base):
    __tablename__ = 'dipole'
    id = Column(Integer, primary_key=True, autoincrement=True)
    tile_id = Column(Integer, ForeignKey('tile.id', ondelete='CASCADE'), nullable=False)
    dipole_number = Column(Integer, nullable=False)
    polarity = Column(BIT, nullable=False)  # 0 = X, 1 = Y

    observation = relationship('Observation', cascade='all, delete-orphan')

    def toDict(self):
        return {
            'id': self.id,
            'tile_id': self.tile_id,
            'dipole_number': self.dipole_number,
            'polarity': self.polarity
        }


class Tile(Base):
    __tablename__ = 'tile'
    id = Column(Integer, primary_key=True, autoincrement=True)
    receiver_number = Column(Integer, nullable=False)
    slot_number = Column(Integer, nullable=False)

    dipole = relationship('Dipole', cascade='all, delete-orphan')
    test = relationship('Test', cascade='all, delete-orphan')

    def toDict(self):
        return {
            'id': self.id,
            'receiver_number': self.receiver_number,
            'slot_number': self.slot_number
        }


class Test(Base):
    __tablename__ = 'test'
    id = Column(Integer, primary_key=True, autoincrement=True)
    tile_id = Column(Integer, ForeignKey('tile.id', ondelete='CASCADE'), nullable=False)
    start_time = Column(Integer, nullable=False)

    observations = relationship(
        'Observation',
        secondary=TestObservation,
        cascade='all, delete-orphan',
        single_parent=True
    )

    def toDict(self):
        return {
            'id': self.id,
            'tile_id': self.tile_id,
            'start_time': self.start_time,
            'observations': self.observations
        }


class Observation(Base):
    __tablename__ = 'observation'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20), nullable=False)
    start_time = Column(Integer, nullable=False)
    dipole_id = Column(Integer, ForeignKey('dipole.id', ondelete='CASCADE'), nullable=False)
    values = Column(JSON, nullable=False)

    anomaly = relationship('Anomaly', cascade='all, delete-orphan')

    def toDict(self):
        return {
            'id': self.id,
            'name': self.name,
            'start_time': self.start_time,
            'dipole_id': self.dipole_id,
            'values': self.values
        }
