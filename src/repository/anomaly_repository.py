import copy

from database.model import Anomaly
from repository import session_factory


def getAll():
    """ Get all Anomalies """
    session = session_factory()
    data = []

    query = session.query(Anomaly)
    for anomaly in query:
        data.append(anomaly)

    ret = copy.deepcopy(data)

    session.close()
    return ret


def getByID(anom_id):
    """ Get a single Anomaly that has a matching ID """
    session = session_factory()

    anomaly = session.query(Anomaly) \
        .filter(Anomaly.id == anom_id) \
        .first()

    ret = copy.deepcopy(anomaly)

    session.close()
    return ret


def create(data):
    """ Create an Anomaly, returns associated row id """
    session = session_factory()

    session.add(data)
    session.commit()

    anom_id = copy.deepcopy(data.id)

    session.close()
    return anom_id


def update(data):
    """ Update an anomaly. """
    session = session_factory()

    anomaly = session.query(Anomaly) \
        .filter(Anomaly.id == data.id) \
        .first()

    if isinstance(anomaly, Anomaly):
        anomaly.type = data.type
        anomaly.observation = data.observation
        session.commit()

    session.close()


def deleteByID(anom_id):
    """ delete an anomaly given an id """
    session = session_factory()
    session.query(Anomaly) \
        .filter(Anomaly.id == anom_id) \
        .delete()

    session.commit()
    session.close()
