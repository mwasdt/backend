import copy

from sqlalchemy.orm import joinedload

from database.model import Test
from repository import session_factory


def getAll(get_obs=False):
    """ Get all Tests
        pass get_obs as true to eager load observations
     """
    session = session_factory()
    data = []

    if get_obs == 'true':
        # eager load observations
        query = session.query(Test) \
            .options(joinedload(Test.observations))
    else:
        query = session.query(Test)

    for test in query:
        data.append(test)

    ret = copy.deepcopy(data)

    session.close()
    return ret


def getByID(test_id, get_obs=False):
    """ Get a single Test that has a matching ID
        pass get_obs as true to eager load observations
    """
    session = session_factory()

    if get_obs == 'true':
        # eager load observations
        test = session.query(Test) \
            .options(joinedload(Test.observations)) \
            .filter(Test.id == test_id) \
            .first()
    else:
        test = session.query(Test) \
            .filter(Test.id == test_id) \
            .first()

    ret = copy.deepcopy(test)

    session.close()
    return ret


def create(data):
    """ Create a Test, returns associated row id """
    session = session_factory()

    session.add(data)
    session.commit()

    test_id = copy.deepcopy(data.id)

    session.close()
    return test_id


def createAll(data):
    """ Create all Tests in a list """
    session = session_factory()

    for test in data:
        session.add(test)

    session.commit()
    session.close()


def update(data):
    """ update a test. Does not update observations. """
    session = session_factory()

    test = session.query(Test) \
        .filter(Test.id == data.id) \
        .first()

    if isinstance(test, Test):
        test.tile_id = data.tile_id
        test.start_time = data.start_time
        session.commit()

    session.close()


def deleteByID(test_id):
    """ delete a test given an id """
    session = session_factory()

    session.query(Test) \
        .filter(Test.id == test_id) \
        .delete()

    session.commit()
    session.close()
