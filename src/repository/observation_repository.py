import copy

from database.model import Observation
from repository import session_factory


def getAll():
    """ Get all Observations """
    session = session_factory()
    data = []

    query = session.query(Observation)
    for observation in query:
        data.append(observation)

    ret = copy.deepcopy(data)

    session.close()
    return ret


def getByID(observation_id):
    """ Get a single Observation that has a matching ID """
    session = session_factory()

    observation = session.query(Observation) \
        .filter(Observation.id == observation_id) \
        .first()

    ret = copy.deepcopy(observation)

    session.close()
    return ret


def create(data):
    """ Create an Observation, returns associated row id """
    session = session_factory()

    session.add(data)
    session.commit()

    obs_id = copy.deepcopy(data.id)

    session.close()
    return obs_id


def update(data):
    """ update an observation """
    session = session_factory()
    observation = session.query(Observation) \
        .filter(Observation.id == data.id) \
        .first()

    if isinstance(observation, Observation):
        observation.name = data.name
        observation.start_time = data.start_time
        observation.dipole_id = data.dipole_id
        observation.values = data.values
        session.commit()

    session.close()


def deleteByID(observation_id):
    """ delete an observation given an id """
    session = session_factory()
    session.query(Observation) \
        .filter(Observation.id == observation_id) \
        .delete()

    session.commit()
    session.close()
