import copy

from database.model import Dipole
from repository import session_factory


def getAll():
    """ Get all Dipoles """
    session = session_factory()
    data = []

    query = session.query(Dipole)
    for dipole in query:
        data.append(dipole)

    ret = copy.deepcopy(data)

    session.close()
    return ret


def getByID(dipole_id):
    """ Get a single Dipole that has a matching ID """
    session = session_factory()

    dipole = session.query(Dipole) \
        .filter(Dipole.id == dipole_id) \
        .first()

    ret = copy.deepcopy(dipole)

    session.close()
    return ret


def create(data):
    """ Create a Dipole, returns associated row id """
    session = session_factory()

    session.add(data)
    session.commit()

    dipole_id = copy.deepcopy(data.id)

    session.close()
    return dipole_id


def update(data):
    """ update a dipole """
    session = session_factory()

    dipole = session.query(Dipole) \
        .filter(Dipole.id == data.id) \
        .first()

    if isinstance(dipole, Dipole):
        dipole.tile_id = data.tile_id
        dipole.dipole_number = data.dipole_number
        dipole.polarity = data.polarity

    session.commit()

    session.close()


def deleteByID(dipole_id):
    """ delete a dipole given an id """
    session = session_factory()

    session.query(Dipole) \
        .filter(Dipole.id == dipole_id) \
        .delete()

    session.commit()
    session.close()


def getDipoleID(tile_id, obs_no, polarity):
    """ get Dipole.id with fields matching params """
    session = session_factory()

    dipole_id = session.query(Dipole) \
        .filter(Dipole.tile_id == tile_id) \
        .filter(Dipole.dipole_number == obs_no) \
        .filter(Dipole.polarity == polarity) \
        .first().id

    ret = copy.deepcopy(dipole_id)

    session.close()
    return ret
