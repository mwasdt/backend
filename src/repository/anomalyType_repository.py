import copy

from database.model import AnomalyType
from repository import session_factory


def getAll():
    """ Get all AnomalyTypes """
    session = session_factory()
    data = []

    query = session.query(AnomalyType)
    for anom_type in query:
        data.append(anom_type)

    ret = copy.deepcopy(data)

    session.close()
    return ret


def getByID(type_id):
    """ Get a single AnomalyType that has a matching ID """
    session = session_factory()

    anom_type = session.query(AnomalyType) \
        .filter(AnomalyType.id == type_id) \
        .first()

    ret = copy.deepcopy(anom_type)

    session.close()
    return ret


def create(data):
    """ Create an AnomalyType, returns associated row id
        expects an AnomalyType or AnomalyTypeView
    """
    session = session_factory()

    session.add(data)
    session.commit()

    type_id = copy.deepcopy(data.id)

    session.close()
    return type_id


def update(data):
    """ update an anomaly type given an AnomalyType object """
    session = session_factory()
    anom_type = session.query(AnomalyType) \
        .filter(AnomalyType.id == data.id) \
        .first()

    if isinstance(anom_type, AnomalyType):
        anom_type.name = data.name
        anom_type.description = data.description
        session.commit()

    session.close()


def deleteByID(type_id):
    """ delete an anomaly type given an id """
    session = session_factory()
    session.query(AnomalyType) \
        .filter(AnomalyType.id == type_id) \
        .delete()

    session.commit()
    session.close()
