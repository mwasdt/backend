import copy

from database.model import Tile
from repository import session_factory


def getAll():
    """ Get all Tiles """
    session = session_factory()
    data = []

    query = session.query(Tile)
    for tile in query:
        data.append(tile)

    ret = copy.deepcopy(data)

    session.close()
    return ret


def getByID(tile_id):
    """ Get a single Tile that has a matching ID """
    session = session_factory()

    tile = session.query(Tile) \
        .filter(Tile.id == tile_id) \
        .first()

    ret = copy.deepcopy(tile)

    session.close()
    return ret


def create(data):
    """ Create a Tile, returns associated row id """
    session = session_factory()

    session.add(data)
    session.commit()

    tile_id = copy.deepcopy(data.id)

    session.close()
    return tile_id


def update(data):
    """ update a tile """
    session = session_factory()
    tile = session.query(Tile) \
        .filter(Tile.id == data.id) \
        .first()

    if isinstance(tile, Tile):
        tile.receiver_number = data.receiver_number
        tile.slot_number = data.slot_number
        session.commit()

    session.close()


def deleteByID(tile_id):
    """ delete a tile given an id """
    session = session_factory()
    session.query(Tile) \
        .filter(Tile.id == tile_id) \
        .delete()

    session.commit()
    session.close()


def getTileID(receiver, slot):
    """ return Tile.id with fields matching params """
    session = session_factory()

    tile_id = session.query(Tile) \
        .filter(Tile.receiver_number == receiver) \
        .filter(Tile.slot_number == slot) \
        .first().id

    ret = copy.deepcopy(tile_id)

    session.close()
    return ret
