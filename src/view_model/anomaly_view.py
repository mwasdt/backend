from database.model import Anomaly


class AnomalyView:

    def __init__(self, anomaly=None, dict=None):
        if isinstance(anomaly, Anomaly):
            self.id = anomaly.id
            self.type = anomaly.type
            self.observation = anomaly.observation

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.type = dict['type']
            self.observation = dict['observation']

        else:
            self.id = -1
            self.type = -1
            self.observation = -1
