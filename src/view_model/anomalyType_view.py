from database.model import AnomalyType


class AnomalyTypeView:

    def __init__(self, anom_type=None, dict=None):
        if isinstance(anom_type, AnomalyType):
            self.id = anom_type.id
            self.name = anom_type.name
            self.description = anom_type.description

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.name = dict['name']
            self.description = dict['description']

        else:
            self.id = -1
            self.name = 'default_anomaly_type'
            self.description = 'no desc'
