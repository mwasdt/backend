from database.model import Tile


class TileView:

    def __init__(self, tile=None, dict=None):
        if isinstance(tile, Tile):
            self.id = tile.id
            self.receiver_number = tile.receiver_number
            self.slot_number = tile.slot_number

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.receiver_number = dict['receiver_number']
            self.slot_number = dict['slot_number']

        else:
            self.id = -1
            self.receiver_number = -1
            self.slot_number = -1
