from database.model import Dipole


class DipoleView:

    def __init__(self, dipole=None, dict=None):
        if isinstance(dipole, Dipole):
            self.id = dipole.id
            self.tile_id = dipole.tile_id
            self.dipole_number = dipole.dipole_number
            self.polarity = dipole.polarity

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.tile_id = dict['tile_id']
            self.dipole_number = dict['dipole_number']
            self.polarity = dict['polarity']

        else:
            self.id = -1
            self.tile_id = -1
            self.dipole_number = -1
            self.polarity = -1
