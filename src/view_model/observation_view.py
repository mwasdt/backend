from database.model import Observation


class ObservationView:

    def __init__(self, obs=None, dict=None):
        if isinstance(obs, Observation):
            self.id = obs.id
            self.name = obs.name
            self.start_time = obs.start_time
            self.dipole_id = obs.dipole_id
            self.values = obs.values

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.name = dict['name']
            self.start_time = dict['start_time']
            self.dipole_id = dict['dipole_id']
            self.values = dict['values']

        else:
            self.id = -1
            self.name = 'default_obs'
            self.start_time = -1
            self.dipole_id = -1
            self.values = []

