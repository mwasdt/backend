import json

from database.model import Test, Observation


class TestView:

    def __init__(self, test=None, dict=None):
        if isinstance(test, Test):
            self.id = test.id
            self.tile_id = test.tile_id
            self.start_time = test.start_time
            self.observations = test.observations

        elif dict is not None:
            if 'id' in dict:
                self.id = dict['id']

            self.tile_id = dict['tile_id']
            self.start_time = dict['start_time']

            self.observations = []
            for data in json.loads(dict['observations']):
                self.observations.append(Observation(
                    name=data['name'],
                    start_time=data['start_time'],
                    dipole_id=data['dipole_id'],
                    values=data['values']
                ))

        else:
            self.id = -1
            self.tile_id = -1
            self.start_time = -1
            self.observations = []

