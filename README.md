#MWA Monitoring Web Service
  
>Monitoring the Murchison Widefield Array Metadata for Antenna Performance and Health  

>Curtin University - Capstone Computing Project One  
Group 9.2 - 2017

##Installation
This installation process is for development only, **DO NOT** deploy using this method.

Ensure you have Python 2.7.13 or greater installed.

Installation Steps:

- Clone the repo.  
`git clone https://gene_cross@bitbucket.org/curtinmwamonitoringteam/webbackend.git`

- Setup a virtual environment.  
`pip install virtualenv`  
`virtualenv venv`

- Start the virtual environment.  
Linux: `source venv/bin/activate`  
Windows: `venv\Scripts\activate`  
Use `deactivate` or close the shell when finished with the virtual environment.

- Install requirements.  
`pip install -r requirements.txt`  
NOTE: Some people have experienced issues when it comes to the MySQL dependency.  
If your mysql_config is missing or broken, try `sudo apt install libmysqlclient-dev`.  
You may also need to update your python install, `sudo apt install python-dev`.

##Database Setup
This application uses MySQL Community Server 5.7.18  
Find the appropriate installation instructions for your system
[here](https://dev.mysql.com/doc/refman/5.7/en/installing.html).

Ensure the URIs match those specified in ./src/app/config.py, (or change them to match your database).

Create the schema on the database by executing `schema_setup.py`  
and seed the database by executing `seed_db.py`.

##Testing
Make sure you're using the virtual environment then 
navigate to the 'test' dir and execute the `run_tests.sh` or `run_tests.bat` file.

##Usage
To start a local development server make sure you're using the virtual environment  
then run the `devenv.sh` or `devenv.bat` file.

##Deployment
Coming soon.

##Documentation
####Dependency information:  
Virtualenv [user guide](https://virtualenv.pypa.io/en/stable/userguide/).  
Flask [quick start](http://flask.pocoo.org/docs/0.12/quickstart/).  
SQLAlchemy [documentation](http://docs.sqlalchemy.org/en/rel_1_1/).

##License
All rights reserved.

